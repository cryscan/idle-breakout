﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BrickGenerator : MonoBehaviour
{
    [SerializeField]
    private GameInfo gameInfo;

    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private Rect range;

    private List<GameObject> bricks;

    public HitEvent hitEvent;

    // Start is called before the first frame update
    void Start()
    {
        gameInfo.level = 0;
        bricks = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bricks.Count == 0) {
            gameInfo.level++;
            Generate();
        }
    }

    void Generate() {
        for (float i = range.min.x; i < range.max.x; i++) {
            for (float j = range.min.y; j < range.max.y; j++) {
                Vector3 pos = new Vector3(i, j, 0);
                GameObject clone = Instantiate(prefab, pos, Quaternion.identity);

                Brick brick = clone.GetComponent<Brick>();
                brick.Life = gameInfo.level;
                brick.hitEvent.AddListener(OnHit);
                brick.dieEvent.AddListener(OnBrickDestroy);

                bricks.Add(clone);
            }
        }
    }

    void OnHit(BallInfo ballInfo, int amount)
    {
        hitEvent.Invoke(ballInfo, amount);
    }

    void OnBrickDestroy(GameObject brick) {
        bricks.Remove(brick);
        Destroy(brick);
    }
}