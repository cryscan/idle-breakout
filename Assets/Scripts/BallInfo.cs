﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BallInfo : ScriptableObject
{
    public float radius;

    public float speed;
    public int power;

    public int priceLinear;
    public int priceBase;

    public int score;
}
