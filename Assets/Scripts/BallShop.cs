﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallShop : MonoBehaviour
{
    [SerializeField]
    private GameInfo gameInfo;

    private Button button;
    private BallGenerator ballGenerator;

    void Awake()
    {
        button = GetComponent<Button>();
        ballGenerator = GetComponent<BallGenerator>();
    }

    void Update()
    {
        // ToggleInteractable();
    }

    void ToggleInteractable()
    {
        int price = ballGenerator.price;
        if (gameInfo.score >= price)
            button.interactable = true;
        else
            button.interactable = false;
    }

    public void Spawn()
    {
        ballGenerator.Generate();
    }
}
