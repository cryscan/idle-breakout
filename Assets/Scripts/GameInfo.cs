﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameInfo : ScriptableObject
{
    public int score;
    public int level;
}
