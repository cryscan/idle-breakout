﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField]
    private GameInfo gameInfo;

    [SerializeField]
    private Text text;

    private bool godMode = false; 
    private bool doubleMoney = false;

    void Start()
    {
        gameInfo.score = 0;
    }

    void Update()
    {
        text.text = gameInfo.score.ToString();
    }

    public void OnHit(BallInfo ballInfo, int amount)
    {
        if (godMode)
            return;

        if (doubleMoney)
            amount *= 2;

        gameInfo.score += amount;

        if (ballInfo)
            ballInfo.score += amount;
    }

    public void OnBallGenerate(int price)
    {
        // gameInfo.score -= price;
    }

    public void OnEnterGodMode()
    {
        godMode = true;
    }

    public void OnExitGodMode()
    {
        godMode = false;
    }

    public void OnEnterDoubleMoney()
    {
        doubleMoney = true;
    }

    public void OnExitDoubleMoney()
    {
        doubleMoney = false;
    }
}
