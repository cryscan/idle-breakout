﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BallGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private BallInfo ballInfo;

    public BallGenerateEvent ballGenerateEvent;

    private List<GameObject> balls;

    public int price { get { return Price(); } }

    void Start()
    {
        ballInfo = Instantiate(ballInfo);
        balls = new List<GameObject>();
    }

    public void Generate()
    {
        ballGenerateEvent.Invoke(price);

        Rect rect = Boundary.BoundaryRect();
        Vector3 pos = new Vector3(
            Random.Range(rect.xMin + ballInfo.radius, rect.xMax - ballInfo.radius),
            Random.Range(rect.yMin + ballInfo.radius, rect.yMax - ballInfo.radius),
            0);
        GameObject clone = Instantiate(prefab, pos, Quaternion.identity);
        clone.GetComponent<Ball>().Info = ballInfo;
        balls.Add(clone);
    }

    public void Remove()
    {
        GameObject ball;
        if (balls.Count > 0)
        {
            ball = balls[balls.Count - 1];
            balls.RemoveAt(balls.Count - 1);
            Destroy(ball);
        }
    }

    int Price()
    {
        return balls.Count * ballInfo.priceLinear + ballInfo.priceBase;
    }

    public void IncreaseSpeed()
    {
        ballInfo.speed++;
    }

    public void IncreasePower()
    {
        ballInfo.power++;
    }
}

[System.Serializable]
public class BallGenerateEvent : UnityEvent<int> { }