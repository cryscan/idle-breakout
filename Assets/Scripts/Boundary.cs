﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour
{
    void Start()
    {
        Rect rect = BoundaryRect();

        EdgeCollider2D collider = gameObject.AddComponent<EdgeCollider2D>();
        Vector2[] points = new Vector2[5];
        points[0] = rect.min;
        points[1] = new Vector2(rect.xMax, rect.yMin);
        points[2] = rect.max;
        points[3] = new Vector2(rect.xMin, rect.yMax);
        points[4] = rect.min;
        collider.points = points;
    }

    static public Rect BoundaryRect()
    {
        Camera camera = Camera.main;
        Vector2 min = camera.ViewportToWorldPoint(camera.rect.min);
        Vector2 max = camera.ViewportToWorldPoint(camera.rect.max);

        Vector2 scale = new Vector2(0.9f, 0.9f);
        min.Scale(scale);
        max.Scale(scale);

        return new Rect(min, max - min);
    }
}
