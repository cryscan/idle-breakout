﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    private BallInfo info;
    public BallInfo Info { get { return info; } set { info = value; } }

    private Rigidbody2D rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = RandVelocity();

        transform.localScale = new Vector3(info.radius, info.radius);
    }

    void FixedUpdate()
    {
        Vector2 pos = transform.position;
        transform.position = pos + rigidBody.velocity * info.speed * Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Brick") || collision.transform.CompareTag("Boundary"))
        {
            List<ContactPoint2D> contacts = new List<ContactPoint2D>();
            collision.GetContacts(contacts);
            foreach (ContactPoint2D contact in contacts)
                Reflect(contact.normal);
        }

        if (collision.transform.CompareTag("Brick"))
        {
            collision.transform.GetComponent<Brick>().Hit(info);
        }
    }

    Vector3 RandVelocity()
    {
        Vector3 velocity = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
        velocity.Normalize();
        return velocity;
    }

    void Reflect(Vector2 normal)
    {
        rigidBody.velocity = Vector2.Reflect(rigidBody.velocity, normal);
    }
}
