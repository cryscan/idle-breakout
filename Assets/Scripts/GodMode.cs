﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GodMode : MonoBehaviour
{
    public UnityEvent enterGodModeEvent;
    public UnityEvent exitGodModeEvent;

    [SerializeField]
    private Text text;

    private new bool enabled;

    void Start()
    {
        enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
            ToggleGodMode();
    }

    void ToggleGodMode()
    {
        enabled = !enabled;
        
        if (enabled)
        {
            text.text = "Enabled";
            enterGodModeEvent.Invoke();
        }
        else
        {
            text.text = "Disabled";
            exitGodModeEvent.Invoke();
        }
    }
}
