﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAttack : MonoBehaviour
{
    [SerializeField]
    private int power = 1;
    
    private bool godMode;

    void Start()
    {
        godMode = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Click();
        }
    }

    void Click()
    {
        Camera camera = Camera.main;
        Vector2 origin = new Vector2(
            camera.ScreenToWorldPoint(Input.mousePosition).x,
            camera.ScreenToWorldPoint(Input.mousePosition).y);
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero, 0);
        
        if (hit)
        {
            if (hit.collider.CompareTag("Brick"))
            {
                Brick brick = hit.collider.GetComponent<Brick>();
                if (godMode)
                    brick.Hit(int.MaxValue);
                else
                    brick.Hit(power);
            }
        }
    }

    public void OnEnterGodMode()
    {
        godMode = true;
    }

    public void OnExitGodMode()
    {
        godMode = false;
    }
}
