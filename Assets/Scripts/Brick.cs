﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class HitEvent : UnityEvent<BallInfo, int> { }

[System.Serializable]
public class DieEvent : UnityEvent<GameObject> { }

public class Brick : MonoBehaviour
{
    [SerializeField]
    private int life;
    public int Life { get { return life; } set { life = value; } }

    [SerializeField]
    private TextMesh text;

    public HitEvent hitEvent;
    public DieEvent dieEvent;

    void Update()
    {
        text.text = life.ToString();
    }

    public void Hit(BallInfo ballInfo)
    {
        life -= ballInfo.power;

        hitEvent.Invoke(ballInfo, ballInfo.power + Mathf.Min(0, life));

        if (life <= 0)
            dieEvent.Invoke(gameObject);
    }

    public void Hit(int amount)
    {
        life -= amount;

        hitEvent.Invoke(null, amount + Mathf.Min(0, life));

        if (life <= 0)
            dieEvent.Invoke(gameObject);
    }
}