﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DoubleMoney : MonoBehaviour
{
    public UnityEvent enterDoubleMoneyEvent;
    public UnityEvent exitDoubleMoneyEvent;

    [SerializeField]
    private int lastTime;

    [SerializeField]
    private Text text;

    private new bool enabled;
    private int remainTime = 0;


    void Start()
    {
        enabled = false;
    }

    void Update()
    {
        text.enabled = enabled;

        if (Input.GetKeyDown(KeyCode.D))
            StartCoroutine(StartDoubleMoney());
    }

    IEnumerator StartDoubleMoney()
    {
        enabled = true;
        remainTime += lastTime;
        enterDoubleMoneyEvent.Invoke();
        while (remainTime > 0)
        {
            remainTime -= 1;
            text.text = remainTime.ToString();
            yield return new WaitForSeconds(1);
        }
        enabled = false;
        exitDoubleMoneyEvent.Invoke();
    }
}
